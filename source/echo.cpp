
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define _WIN32
#include "echo.h"
#include <axiom_xml_writer.h>
//#include <Windows.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

//face rec
#if defined(_WIN32) || defined (_WIN64)
	#include "stdafx.h"
#else
	#include <sys/stat.h>
	#include <errno.h>
	#include <dirent.h>
#endif

#include <iostream>
//#include "ScoreCheck.h"
#include <vector>
#include "NeoFacePro.h"
#include <errno.h>
//#include <Magick++\Include.h>
#define cimg_OS 1
#define cimg_use_magick
#include "CImg.h"

using namespace cimg_library;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define FILE_NAME_MAX 200

/////////////////////////////////////////////////////////////////////////////

#if defined(_WIN32) || defined (_WIN64)
	CWinApp theApp;
#else
	#define strcpy_s(a, b)                        strcpy(a, b)

	#define ZeroMemory(a,b)                   memset((a), 0, (b))
	#define LPCTSTR            char*
	#define CString            std::string


	typedef struct tagBITMAPFILEHEADER {
		WORD    bfType;
		DWORD   bfSize;
		WORD    bfReserved1;
		WORD    bfReserved2;
		DWORD   bfOffBits;
	} BITMAPFILEHEADER;

    char* CT2A(LPCTSTR x)
    {
	    return x;
    }

    const char* CT2A(CString x)
    {
	    return x.c_str();
    }

#endif

using namespace std;

// Using NeoFacePro namespace.
using namespace NeoFacePro;

// Class which contains file names of features.
#if defined(_WIN32) || defined (_WIN64)
	class feature : public CObject
#else
	class feature
#endif
{
public:
	feature(){
	}
	CString fileName;
	CString name;
	CString fetFileName;
};

//end faCE REC

/*
#include <iostream>
#include "ScoreCheck.h"
#include <vector>
#include "NeoFacePro.h"
#include <errno.h>*/

//#include <stdint.h>
//#include <stdlib.h>

//#include <iostream>
//#include "ScoreCheck.h"
//#include <vector>
//#include <NeoFacePro.h>
//#include <errno.h>
//#include "base64.h"
//#include <iostream>

bool EnrollCompoundFeatures(vector<feature *> &Features, char* lpPath, int iAlg);
bool LoadFeatures(vector<feature *> &Features, char* lpPath, int iAlg);
//recognition_data *Verify(vector<feature *> features, int iAlg);
recognition_data *Verify(int iAlg);
void DeleteArray(vector<feature *> arrayFeature);


#ifdef __cplusplus  
	extern "C"{ 
#endif

axiom_node_t *build_om_programatically(
    const axutil_env_t * env,
    axis2_char_t * text,
	axis2_char_t * text2,
	axis2_char_t * text3
	);

void set_custom_error(
    const axutil_env_t * env,
    axis2_char_t * error_message);

void filecopy(char *fn, char *bn);
float max_array(float a[], int num_elements);
//moved these 2 header file later
void build_decoding_table();
void base64_cleanup();


//base 64 decode/encode stuff
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};


char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {

    *output_length = (size_t) (4.0 * ceil((double) input_length / 3.0));

    char *encoded_data = (char *)malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? data[i++] : 0;
        uint32_t octet_b = i < input_length ? data[i++] : 0;
        uint32_t octet_c = i < input_length ? data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';


	char sadasd;

    return encoded_data;
}


unsigned char *base64_decode(const char *data,
                    size_t input_length,
                    size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    //if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = (unsigned char *)malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
                        + (sextet_b << 2 * 6)
                        + (sextet_c << 1 * 6)
                        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}


void build_decoding_table() {

    decoding_table = (char *)malloc(256);

    for (int i = 0; i < 0x40; i++)
        decoding_table[encoding_table[i]] = i;
}


void base64_cleanup() {
    free(decoding_table);
}

//end base64 stuff


/*new base64 stuff*/
/*
** MIME Base64 coding examples
**
** encode() encodes an arbitrary data block into MIME Base64 format string
** decode() decodes a MIME Base64 format string into raw data
**
** Global table base64[] carries the MIME Base64 conversion characters
*/


/* Global data used by both binary-to-base64 and base64-to-binary conversions */
static char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789"
"+/";

/*
** ENCODE RAW into BASE64
*/

/* Encode source from raw data into Base64 encoded string */
int encode(unsigned s_len, char *src, unsigned d_len, char *dst)
{
unsigned triad;

for (triad = 0; triad < s_len; triad += 3)
{
unsigned long int sr;
unsigned byte;

for (byte = 0; (byte<3)&&(triad+byte<s_len); ++byte)
{
sr <<= 8;
sr |= (*(src+triad+byte) & 0xff);
}

sr <<= (6-((8*byte)%6))%6; /* leftshift to 6bit align */

if (d_len < 4) return 1; /* error - dest too short */

*(dst+0) = *(dst+1) = *(dst+2) = *(dst+3) = '=';
switch(byte)
{
case 3:
*(dst+3) = base64[sr&0x3f];
sr >>= 6;
case 2:
*(dst+2) = base64[sr&0x3f];
sr >>= 6;
case 1:
*(dst+1) = base64[sr&0x3f];
sr >>= 6;
*(dst+0) = base64[sr&0x3f];
}
dst += 4; d_len -= 4;
}

return 0;
}

/*
** DECODE BASE64 into RAW
*/

/* determine which sextet value a Base64 character represents */
int tlu(int byte)
{
int index;

for (index = 0; index < 64; ++index)
if (base64[index] == byte)
break;
if (index > 63) index = -1;
return index;
}

/* Decode source from Base64 encoded string into raw data */
int decode(unsigned s_len, char *src, unsigned d_len, char *dst)
{
unsigned six, dix;

dix = 0;

for (six = 0; six < s_len; six += 4)
{
unsigned long sr;
unsigned ix;

sr = 0;
for (ix = 0; ix < 4; ++ix)
{
int sextet;

if (six+ix >= s_len)
return 1;

/*if(*(src+six+ix) == 10) {
	++ix;//increment when a space has been detected
}*/
if ((sextet = tlu(*(src+six+ix))) < 0)
break;
sr <<= 6;
sr |= (sextet & 0x3f);
}

switch (ix)
{
case 0: /* end of data, no padding */
return 0;

case 1: /* can't happen */
return 2;

case 2: /* 1 result byte */
sr >>= 4;
if (dix > d_len) return 3;
*(dst+dix) = (sr & 0xff);
++dix;
break;

case 3: /* 2 result bytes */
sr >>= 2;
if (dix+1 > d_len) return 3;
*(dst+dix+1) = (sr & 0xff);
sr >>= 8;
*(dst+dix) = (sr & 0xff);
dix += 2;
break;

case 4: /* 3 result bytes */
if (dix+2 > d_len) return 3;
*(dst+dix+2) = (sr & 0xff);
sr >>= 8;
*(dst+dix+1) = (sr & 0xff);
sr >>= 8;
*(dst+dix) = (sr & 0xff);
dix += 3;
break;
}
}
return 0;
}
/*end*/

axiom_node_t *
axis2_echo_echo(
    const axutil_env_t * env,
    axiom_node_t * node)
{
    axiom_node_t *text_parent_node = NULL;
    axiom_node_t *text_node = NULL;
	axiom_node_t *image_node = NULL;
	axiom_node_t *image_text_node = NULL;
    axiom_node_t *ret_node = NULL;
	axis2_char_t *text_image_data = NULL;
	axis2_char_t *text_badgeNumber = "sasadad";
    axis2_char_t *text_str = NULL;

    AXIS2_ENV_CHECK(env, NULL);

    /* Expected request format is :-
     * <ns1:echoString xmlns:ns1="http://ws.apache.org/axis2/c/samples">
     *      <text>echo5</text>
     * </ns1:echoString>
     */
    if (!node)                  /* 'echoString' node */
    {
        set_custom_error(env, "Invalid payload; echoString node is NULL");
        return NULL;
    }

    text_parent_node = axiom_node_get_first_element(node, env);
    if (!text_parent_node)
    {
        set_custom_error(env, "Invalid payload; text node is NULL");
        return NULL;
    }

    text_node = axiom_node_get_first_child(text_parent_node, env);
	
    if (!text_node)             /* actual text to echo */
    {
        set_custom_error(env, "Invalid payload; text to be echoed is NULL");
        return NULL;
    }

    if (axiom_node_get_node_type(text_node, env) == AXIOM_TEXT)
    {
        axiom_text_t *text =
            (axiom_text_t *) axiom_node_get_data_element(text_node, env);


        if (text && axiom_text_get_value(text, env))
        {
            text_str =
                (axis2_char_t *) axiom_text_get_value(text, env);
            //ret_node = build_om_programatically(env, text_str, text_image_data, text_badgeNumber);
        }
    }
    else
    {
        set_custom_error(env, "Invalid payload; invalid XML in request");
        return NULL;
    }

	image_node = axiom_node_get_next_sibling(text_parent_node,env);

	if (!image_node)             /* actual text to echo */
    {
        set_custom_error(env, "Invalid payload; text to be echoed is NULL");
        return NULL;
    }

	image_text_node = axiom_node_get_first_child(image_node, env);

	if (axiom_node_get_node_type(image_text_node, env) == AXIOM_TEXT)
    {

		//image data
		axiom_text_t *img =
            (axiom_text_t *) axiom_node_get_data_element(image_text_node, env);

        if (img && axiom_text_get_value(img, env));
        {
			text_image_data = 
				(axis2_char_t *) axiom_text_get_value(img, env);
        }
    }
    else
    {
        set_custom_error(env, "Invalid payload; invalid XML in request");
        return NULL;
    }

	ret_node = build_om_programatically(env, text_str, text_image_data, text_badgeNumber);

    return ret_node;
}

/* Builds the response content */
axiom_node_t *
build_om_programatically(
    const axutil_env_t * env,
    axis2_char_t * text,
	axis2_char_t * text2,
	axis2_char_t * text3)
{
    axiom_node_t *echo_om_node = NULL;
    axiom_element_t *echo_om_ele = NULL;
	axiom_node_t *bn_om_node = NULL;
	axiom_node_t *bntxt_om_node = NULL;
    //axiom_element_t *bn_om_ele = NULL;
    axiom_node_t *text_om_node = NULL;
    axiom_element_t *text_om_ele = NULL;
	axiom_element_t *bn_om_ele = NULL;
    axiom_namespace_t *ns1 = NULL;
	//FILE *fi = NULL;
	char byte;
	size_t length = 1;
	size_t *pOLength = &length;
	//unsigned char * dec_data = NULL;
	char dec_data[100000];
	//char dummy;
	//char dec_data[1000000];
	axis2_char_t * error = NULL;
	int index;
	char scoreArray[20];
	bitmap_header* hp;
    int n;
	recognition_data *pdata;
	float _s;
	axis2_char_t * _bad;
	

	//encoded string
	//char *data = "/9j/4AAQSkZJRgABAQEASABIAAD/4QCARXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAKgAgAEAAAAAQAAAGOgAwAEAAAAAQAAACQAAAAA/9sAQwAGBAUFBQQGBQUFBwcGBwkQCgkJCQkTDg4LEBcUGBcWFBYVGRwkHhkaIhsVFh8qHyIlJigoKBgeLC8rJy8kJygm/9sAQwEHBwcJCAkSCgoSJhoWGiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYmJiYm/8AAEQgAJABjAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A8uoorZ8IaOmva/b6XJO0KzBiXVckYUnp+Fek3bU8JK7sjGoqzfWpt9SuLKMtIYpmiXjlsHA4rr73wBcjX7PRrG53yS2ouLh5V2iAZIOcfSk5JDUG9jh6K9A/4QTSb3zbXRPFNvealEpJgKhQ+OuDn/GsPQfDX9o2WvTXM0lvNpMJcx7M7mG7IPp92lzIfs5Xsc3RXWeGvCA1HS21nVdSi03S1basrjJkPsPrUul+DYdSu7+4h1aKLQrN9pv5VwH4B4HHr/KnzIFTkzjqK7TV/BUC6TPq3h/WYdUt7YZnVV2ug9cZ/wAKng8F6PaadZ3HiHxGljcXkYlSFYt2FPTJpcyH7OVzhKK6LxPpXh/T7aGTR9e/tCV3w6eVt2jHWudqk7kNWdgooooEFdd8Kf8AkebD/dk/9AauRqW2uLi1mWe1nkhlXo8blWH4ik1dWKi7NM9OuvGugaL4kmgsvDVu0UM7CW6480tn5mGRnrnv+Va7pDB4s1mwm1FgfEdgHtJ5Dyhww2frwPQY614s7tI7O7FnY5Zickn1qW5u7q6ZGubmaYxrtQySFto9BnoKnkNFWfU77wt4I8T6X4qsLqeyAggnBeVJlI29z1zjHtWtCAL74j4GP3J/9BevNY9c1qNAkesXyqOgW5cAfrUH9oX4a4YX1xm5GJz5rfvf97nn8aHFvcFUjFWSOz8Ykr8PfCSA4UrISOxPHP6n86s+Hrc+JPh1LoGnTIupWlz55hZtvnLz/j+grgJbu6mgit5rmWSGH/Vxs5Kp9B2pkM0sEqywSvHIvRkYgj8RT5dBc+tz0zwto954O0nWNX1/ZbpPbNBFbFwzSsenAP4fiam8O3fijUPD9iZfCdjq1vEmy3nnZN20HGOT7Y/CvMby9vL1g95dz3DDgGWQuR+dT2+satbQrBbapeQxJ91I52VR9ADScGxqolotj0TxtplufBsmo6noNpo+pxzqkCW7L+9U4znb7Z/KvLasXl9e3rK15eT3BXoZZC+PzqvVRVkROSk7oKKKKZAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAf//Z";
	//char data[] = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0a HBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIy MjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAHgAoADASIA AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3 ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3 uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDQfwzo GP8AkCaaB/16J/hUDeHNBBP/ABJdOx/16p/hWxI3Uc5qrI3PfFapJIzRlHw7oWT/AMSfT/8AwFT/ AAqJ/D2ijkaPp/8A4DJ/hWo2RUZyT6+vNSO5lHQNFJP/ABKLD8LZP8KafD+jdf7Jsf8AwGT/AArV K88U3GM9aXUbuZR0HRgf+QTYf+Ayf4Uh8P6P20mx/wDAZP8ACtT1zg0EZzT+ILmS2g6P/wBAmx/8 B0/wpv8AYOkH/mE2P/gOn+FapGOKaeKLLqBmf2Do4HOlWP8A4Dp/hSHQtH7aVYj/ALd0/wAK0+KQ /pTbVguZX9g6Rn/kFWP/AIDp/hQdC0j/AKBVj/4Dp/hWoR6dKYRhqGkFzMOh6SP+YXZf+A6f4Uv9 h6RnH9l2XP8A07p/hWiRmk2nPWlYLmadD0np/Zdl/wCA6f4U06JpOf8AkGWX/gOv+FaRFNK4NFhm d/Ymkj/mGWX/AIDp/hSHRdK/6Bll/wCA6f4Vod6Sk1Zhczzoulf9Ayy/8B1/wpDomlD/AJhtn/34 X/CtA9KTFMDP/sXSj/zDLP8A78L/AIUHRtL/AOgZZ/8Afhf8Kv8AekxQFzP/ALF0v/oG2f8A34X/ AAo/sXSv+gbZ/wDfhf8ACr/egimttgKH9jaV/wBA2z/78L/hSf2Lpf8A0DbP/vwv+FXzSYqtBGed G0z/AKBtn/34X/CkOjaZ/wBA60/78L/hWh35pp5z7VIyh/Y+mf8AQOtP+/C/4Un9j6Z/0DrT/vwv +FX8UFaaSEURo+mf9A60/wC/C/4U06Ppuf8AkHWn/fhf8Kvk4pOvNLS4XKP9j6Z/0DrT/vwv+FJ/ Y2m5/wCQfaf9+V/wq9S07BdlA6Pp3/QOtP8Avyv+FJ/Y+m/9A+0/78L/AIVfPWm5p+oFH+yNN/6B 9r/35X/Cj+yNN/6B9p/35X/Cr3ApcCm0gTKH9k6b/wBA+0/78r/hR/Y+mn/mH2n/AH5X/CrxGKaK SDoUzo+m/wDQPtf+/K/4Ug0jTj/zD7T/AL8r/hV3rSgc01EOhROkab/0D7T/AL8r/hSHSNN/58LT /vyv+FXsUAc0krBcpf2Rpv8A0D7T/vyv+FH9kab/ANA+0/78r/hV3NHWmkCKf9kabj/kH2n/AH5X /CkGk6d/0D7TH/XFf8KvD36Uhx2oSswKf9kab/0D7T/vwv8AhSf2Rpv/AED7T/vyv+FXO1OFFtQK X9kaaP8AmH2n/flf8KBpGmf9A+1/78r/AIVd60DrTUeoFQaPpvfTrT/vwv8AhQdI03P/ACDrT/vw v+FXRSEY5pWQJlP+yNMH/MOtP+/C/wCFB0jTMj/iXWn/AH4X/CruaXGRRyrcLlaPRdLJ/wCQdZ/j Av8AhUn9jaV0/syz/wC/C/4Vaj6VJUtDKX9i6V/0DbP/AL8L/hR/Yulf9Ayz/wC/C/4VePSgYqbA Z50XS/8AoG2f/fhf8KRtF0vtptn/AN+F/wAK0DTT3q1GwHM6jpNinKWduuD2iUf0rIaxtd2BbQ/9 8Cum1JQVYNnHtWE4GK0hFbslsqNZ2qnH2aIn/cFILO1Jz9ni/wC+BVgEKpPftTM4NbxStqiLsiay tj0t4h/wAUwWVvj/AFEX/fAq0TgU3jNCggTK/wBjtunkRf8AfAoNlbk/6iL/AL4FS8ZqQMD3pKKv sVcqm0tx/wAu8X/fApRZ2xH+oiz/ALgqduSTSZAFNwTBsri0tyceRF/3wKPsluD/AKiL/vgVMBzk dKUgHkdKfJFjuQi0tv8An3i/74FJ9kt/+eEX/fAqfoKVcZ5NHs0Fyv8AY7c/8sIv++BQbS3x/qI/ ++BVjIBOOlIeRzQoRSFdntznjrk5qJ6kJznFMIyOa4tCiE9KjI71M6Y5FMC81D7hYjI96Tgkjpip GXJpoUZ9KpaoGrkZwDimEfNUpxmmkelFwsRsuKbj2BqRuM1GeKWgWEI44ppODT8cU1gCT0x2GaL3 YxhP5UmAeSaUjPI6UmCaBDTzTCfrTyMZppHOTTUU1cBDTCKeRTeKGhjCMUDmnUmMU9AGmkIyKU0m c1NgGYxS05ulNzQvMQmKSl6mkzzVNOwCfSjoKCcUUWAb3pCaXFGBRYBppCeKUjk0mPWhabAIQTSd KXNHWnYLCGkNOpCaEIaKO9LSDmqHcaRigGlx6mjHtRcApDijvRx3p2YCCnHpmkIwKBU2d7gAHPPe gjBpM9aCc00uorBxR9KKMUDDrSgA9aTpTuPWgAxz7UhGKMml7c07CsLik28UZyKUA/hQr2GAGKDg 0hINHOcULVXAUg+lKKXtQeaN1YLEkfB9qk61FGe3aphipsxgKQ8Gg0YNLbcYA0h4BNL0NI3Si9gM jVASmc81gkgk5roNTAaI49M1zjDBNdFJpxJkgZgTgj6VGRtFKTg5BprEmrWxCQfWgn0ppPvQTVRb 6hbUKXtSDnvSEYFNvsOw/cPQU09OlGM0EYPWhOwCgfjRgYpCaMZFDWtwE6ngUp44pRwOKYetJ3uU loLjilxTck9TSjg0/Qk9wIHXNMIwM1MQO/61Gw5/+vXnbFW6kWD+PammptuQBmo3yOlCdtwIj/Kk x3p/uaaRihtdAsRsAvOeO9NzyeakOCcUxwOcH86avawyM03FSEcUzB59aQDDimnmpCOaYRTTXQCM gUnSnlabjjmpV7gNYZ570w8U9vY038Oaq9tBjT196aRzTzTTxSQhhFJ0px5pB+tO9kA003pTvrTS aLaABppFOIyKb0p2aAPrSdaO9Bou7gNxRS4pMU2K4hppPpTiKbQlYBKTrTjSEYpbCuITgUnagikq tOgwNJincUxnUDJP60mAtNziqst2FzVOXUCBwT9KLgrmoXCnk0jTJ/erAlv2IODiqzX8meHp3Ksd OHU96cCM461ya6jKG+/VyHV2X7x/Knr0E0dCWGKQdKy4tWjYjJ61eju45DhTU3FsiU9aU8Um4HpS jFWtgCnDApDilABFKwB16UmKXO36UnWgApe1H86MUAJTs0AcUcE4p3sAACjFGOOKBmknrcAB9aUc UlKBRsND061OBxUMeM1NQ2AnfmlzSCjOalgBOBSUoNNY8Gh2uMz9SAMTHtg5/I1zMpAYrx1rqdRX Nu2OvauTY5J5zk1pTV00D2I2PNJuPSg4pBjNdC2sQGc0daOnWgYo1QCjFBJ4FNJpd3FG2obBignt SfWjmqvoMUD8BQKAeOaQ4obSFcM5pwxjJqMn0pQePekmA7INJketGe2KUbe5x70OUlsguz3YjI68 ntUbKMnBGamIyc80w859a8567jsQk4prYJPNSNg/hURHPNC03Cw0jI64pjDjHNS460xhRdtjsRFc c001JnOaYeDQnYdiMjFNIp55PvTCO1F77iYw8cU3+VOIyaa3Smmh9BG5NRsKfmmn60MkZTSc04mm nmhDEpOtLTSvbvR1AQ9KYevFOPFNPWreqEITSY9KXFIaBiEE96Q9aXGKQ80LzEN6HigkmikxmkAZ pDS4pDzTtqAU0inGkNDTuK40jHNBpQKQ9OaW4WG4z3pCQOtIzbRk9KqTXHUCqSVrsaWo+e5VRgda oy3JOcnj61FPIwzz1qjLIam5SQ+WbJOKqyy8Ypskmc9qruc00mN6iPJxjNRZoJ96aTU21ELnvSb6 aTmm5ou07oCUOc5qxHdPGwIY1TzRmmpAb9trBUjzMfWtm3u450DButcUGq1b3UkLAqTj0qk7oTO0 Xn6UHrWRZ6qsgCscGtRXDgEGmk0TqP7UfSgHmg09higGg0g4NLSAM8UL1opcgChgLkUlHBoHpQlY WwA5paSlzTGPj+8KmI5qAZyKnBqWMdjim45oyR9KDUsYmKQnFL+NNI44oAqX3MDYxwDXJzkiRuMc 11t0MwtkVyl4cSsPetaUugpFakpG6cUmeOtb2EOJopuO9ByaE+whcc0E84xRQBzTaeyGGaM5o78U g4J9KdmhC5petN6mlovcVgIzSYOeKdzSA80bMYuMdaDSk8U0HBGRkVXMC1PfGxzj1qNuCalb2qMj JHpXltstETYpp69akkGMDFRnB609dwI8dc0xvSpGwDTGOM0m+rAiPNNNPYg0x8YwOPqaSeug+g08 H61GeTmnkYX3puarW92LQjJ54pp+tPc4pnXmi6YDWphGae3rTT0p3shDDxTacaaaOW4CbutITS9K aeaYDTmm040hpK/UBM02loPFO/YQ0nHFGMUo5ppNPYBCcmkoo607gJQRRQTS5mA0mjPNIRige9F7 7iAnmmMQASacxAyfSqU9xkYHShhuNnmyMA8VQkkx+NPkftVKSUnrS33NEhsjknnmq8jA5NEsvHWq xfJIpxg31ENkaoi1OYZ5phFaWktxCHmmnpSmmk4qGwEJFNpTzSVmMKXNNNGaoB2acr4qPNFLVAWE kwQQea1LHVmiYJIcqfWsQNilDc5rRTdtQO7hmWZAyHIqXrXJ6dqbwPsZvkPGK6eCVZow6kEGnqJo mHBpRSDGaXNMQuR3pOtJ9aTPNIEPxikzTgR60mBmi4hRQxxQCKTrQrX1GOXORU6/pUC9RVgHCipb b2BAe9FBNJmp6WKENB4oJxTSSaTbCxBdHMRrkrwhZ39M11twP3Zrkb8YuGPc1rSTvoNx0KjHnjpR jNA5o6VvZozsKTSZpDS59BVK4wBoLUA0ECk+ZbAKKKQelIDiqu+oWFOaUUm/NG7tSQhTR25ozkc5 pCfSndghelL1puSKAcnmnfQD39hzwaY1PbnNRv0rzXdmgxjnNRNznPapCQAaiJ64x1pXewW7DDyf amsKcSajLYzU21ENJH40wkk+lOJ496jJ44/nVJWYxGx3P60xxgUpJ554prHPWqv0FYYeaQgY604m mMfSlYBpOM+1N60uaQnFPUQxjzSUp60hPFUhjc0hpSab9aOYQhPrTetKaQnFCWoCGkNB96O3Wk2u gCEmm4PWlJzSZpgIaUdKTPFHSkhWE560lKaKuwhtITgGnZ9ailfahIqWBXuZdvA61QY55NOkkLSH JphNJtPQtIrzPgnmqE0hySauXBxkVmTHLURabsVdWI2bcaj6GnE8VGTWqsiWxWbNMJpxppqXd9QE phFPzSGk3dCGYpDS0hpdBiUGikxU6gJRmg0ZxTAKUGkoFADw2DW3o+o+XIsTn5TwKwc0+Nyjg+9a QaWjEegqQ2KdnHUVmaVdme3UE5IGDWlnIqnoSO7UgxxRu4xSE5PTikNCkY6UYpDk0daTeoCgevSl 70gGaAOadxjgeeKnU8Cqwzn2qypwBSbAUjNAFFHU1FxjT1pCOaU80wnHGaTYEc+BGfpXJ6kP9INd ZINykHiuW1QDzz0z/wDXrWi7SG3oZ3TpSE80EkU01pJ2d7kjqUHFNzSVXPdJoVhxOMigUY4pKcU0 7iHGgU3NLWl2x2F4oApMUZ4o0AdkCjGTSDFLUvcQbaMUjHFKpzRcLHvrcHrx6VE5OakJ5Jz2qN8j Iz3ry9UtTQj69/wphwc/1pXY9BxTD064pXfUdhjYpjHGacRzTS3WmndisRk80w4zTicnjvTCeaN2 A1qYRwaeeaa2KfXYBh70hoNNPNWriE9xTSQKUnA4pvSnewhDTT2pxppoTQXGE0EcUHNNJo6ABFNN OzTTindiEPNHeg9KQnmgYhpM0pHrTaADjNITzQaKdxAOtBxRmkou7AFULyTjaDV1yFUmsm4Ys5z2 oewJEK5J5/nTXYevSngVFKBjOayb6miKszDnNZ0xG7ircpz1qmxyacVbVCkRt0qOnuc001snZWJE 4pDQRRRZsQlBpcUhFDelh3GEUlSYppAFSwuMIpKeRTcUmtBjTSGnGmkUr2ASkzS4pKLgKDk0vem9 KcKANnQ7jZNtzjNdWuWArhLOQxXKMOxrtoHDRKc84rRO6Je5KOuKXGKTvS00n0AKM0uaTNSr3ATv 7U7jvTeKCKp6jHgjOKmTpzUC4zVhDxj0pPYEKaKMUuKzuMYf0pp5p5prYp3Hcik5BFczq6/v8jpX Tt0IFc7rAy1ODtK6DoYppvSnkUzFdErtbECijApMUopRetuo2LSdaTGTSgValK9rCDFFFLjNVzPZ AGaKCvNGBmpjJp2aEH4UuSaXApRjHfNXsrhcYeaUcjrSE4NAqL9WB74xABGaidvr1pzkYOOaYxBP brXmp6mtkRuOetMJ4xTnJzxzUee/86bsFxGPpUWeTk4p5PzcVG/OaSugsIxwTimHpSnPTrTD1qkh eQHnp1phJpTz3phq1othDWpCeKCaQmk5ANJpC2DQfrSHrVK71AQ96aT6049KaaVgEJ4ptKfakzVb CEppxSk4pOtLQBCKTvS5ANJnvT9QDHFNIx3paXjHvTbEMxRilNJ1pAJRRijoKoTZFOcIazH5Y5rR n+4az3XOame10XEjIqKVQy81Pio5R8hrPfcpGRcY3FR+dUyKvTp8xwarlQDWvInawmVtpzSbalY4 NNGDWqgRcbjIphU1KOKDjnvVcvNoK5CRRinlabtNQ4paDExSFc08DFIRihpNXsFxhFNxTmNRl6TV 9x3A4FNJpC1NzUNp7DFPWkApKOlIBxFJQDzTqAHRnDCuz01w9spz2ri14rrdDYfZMdwatWaEaoU0 uKMmgU7tCF6UhozmlpeYCYpT0ApM0p65qtRgOoqdOmKgHUVYjPFRIEOxRmg0hNRZjENNbpTiccUh 6Ur62GRniuf1hDkH07V0TVhaupKE+lWnZjuc+wzTCMU585pvWt+ZdCA5NHSl6UU4pXASloIoFNQa eoBjFAopRjNUmugheaTFKTSdadu4BilUUelGKd0tgDFI3HSge3SnAjHNJ2kgPdicg81E2e/TFOY8 GoyeDxXlt6aGohbrmo2bPrS7uaYzAHH5UrvsKwjHFRs/FOJIxmo2ORmr3QWG5waD0o6delN3U0+4 noNY80wninN1phxTuIaTSHkUuQKaTzSGIeKbnNKTmmnvTuIM00nmnGmGr6iuBPpTTS0h6UNMBpoo JpD7UPUBCKSnEUw0rW1QDjxTc0p6U2r3AXrSZozRjila+pLEoJ4o7UlVfQbIp/u1QIq/MPkPrVE9 TUSeg4oaR6VDL096nYcVnX03lIazScnYspXEqqTVF5uTUc0hdutQkHFdKjykMkaamiQ0wLS7RmnG T3uIk35pwNR7acKqNR31ETdRRwKYDQTWujACTTWal3UxmFS2rWTAjY5NMpxNJ1rnbKG4oxTqKTSA bikIpxpM4qQG9Kdmm5opDHg811GgvmBgeeePyrlxXT6EMQ8etaQejJkbmOKCAKTBop+YhQcUE56U lL0p2Qw6UvXik60HNKSuA4DHerEf3arLU6fd61LGh59aaTj60tBFS0wEIpOlL9KQ80WKuRucA1j6 sp8pq2WNZOrLugb6VMfiGc0xyTTDSsMGl4rqt0ZA0UmaMZo6U9YoQuaKQUvWmve3YAaUYxSUVfIr gBNOBptKMmi9mAuaUGmYpRx1NSpPqhC5FLwaZnml71okrWBHuTHPp+dMPSnkUxsA4BrxkbEbD8aY Tinmo2/SqARuR71GetOY80w1WjQCMc0zODTj6Uw9aN9BMaT1prHJ9qc3NMPAx3ppO4WGmmk8U5va m44p+QhO1ISOaU5FNPAqrCEGehpP1ozikIp7MVgNNPSlzmmmhvQBKQHmlJop2aAQ02nHFNo1uIKS lpKeoCdKWim0uoMWkNLSHiq2JI5BlTVDHzc1oucoRWc4IkNRIuLEbABrD1EszkDpW3IflIqjLArE kge9RB8ruaM55kI5NNIq7doqHiqRNdEZJ7mbTGEYpAaRmphPNJ2ES7qAajBqRTmndvYZIppxpq0E 1rFuK1ENY1CzU9jUTVjJq90MQHJp/FRZxTg1QgH4zSHip4FVgdx5psuzHA5rZUk43uK+pBupCc0d 6DxWIxKM0lLSGPU811WhjNvkdjXKLxWpZam1sAoxiqhcTOwzS8VSsb1bqIMOverlX5E2AYNJ3paK YxRR160YpOBU7gB68VYjPYVBxU8R4FD2Gh560h680tBHFRYY08Uhpev1puKVwGtg9azdTH7lgPQ1 pkZqjqC/uT9KXUZyL9SKaTUkoxIfrURrp59NSWgzSZoopOTkgClBxRikoi2ncQ7OaCKTFFa8/NoA uOKAKTpSjmnyK6YMKUc0EUYqtbiFOBxRikxQM0tbjPcSc8d6YxGetOI5phz6/ka8lppWbNvQYx5q NjjinkA96Yx4ND1QDCeT601vSnNjHB/Goz1znNXZ7AJ35prEU5hkUwnA60K6epI05phOO9P6/SmH 61TugG8E8mkJpeB3pOnemm+ohCaYTTjTTzSbaENNITTjTSMU1dgJSYpSaSrS0EIcflSE5paTgClc BDSZpTzTaabEBo5FKKDVWYDaO9FIKWwhc80lHek60wGsfWqUuBIeaszyCNazBN5r5ByKl2sVFCzO Kpy3AjXI6mpbliBWZMWk4qVC+peyKtzIG56mq4iaQbgOKtrApznknpVckoeDWsU76kNtlZ1K5BqM DmrTfNyetRlRVOLeqFci704daXZT1WpfNYdxV4pSMUuMUhpq9tQI2qNhUjCmEVFmMiIpQacRTKWw EisRQ3NMBpc8dafM7WEIAaDS4yKMZpDG4op2KTFFgClzikApTSA3NAmYSshPy4rqAeBXH6M228QD qeP0rr14FaLYTFxRSgikpiCjFFLS1C4mani4FQVNEaGBL26UmSeKXpSDk1N7FDelJg07qaQ0mNCG qV8Mwt64q5Va75gI9anW47HHzH9431qEmp7iPbKwHTPFQkcV0JNksSgg0o4pM1fKtmITNA5pcUdK z15hB0ozSkUgFata6AFKKMc0VpZvUBcmlHTApMUgBzU+8ughRS0hGKB1q1cD3E4K4FRNketSsajb mvGaTOjoRZphNSsMVGw/OrurWQiNuR/9em9BTzjv19aaad7CGMRnBpjCpD+lMPBp+YhhqMjFSMaj JzTsHQaeaQ0ppDxTW4hnJpDkU48Zpp96N9xCdKbnPenEUnrVWSQCHimk8UpGaTFAhM4pOPwpT3pM ZFACE0hHFKaBVK6QhKSlNJTTuFw4pD1oNFS27iYUmc0uaTNVuIpXx/dkd6yrTIJHvWrfYKEj0rNg TGTQ2rWNobDbsZXg/Ws9vlrUnTcvHWqM0fy5rG7WiQaFCVuKrMxqxN1qs1dUZWVmQxuc0EUoFKBS U9LCCNNxxUvlbetSWyZbpUs0ZxnHNT7zKsVSuaYwxVgRk0x4jWnKmhXSK7DNRGrBXFRMozUuDC5E RTCKmxTGFZvTcYyjNIaXFK4DgeKWmilzTTsAUhpRSGjQBAaXOaTNKOlAGhpGPtsf+9XYxkbBn0rl NFUNdCutUELjitLaEsX3pOtGKXFLYApe1NHFLmiwWA1JFUealhHPWlLRAibHFIBQRRyKi9xiEYNI eRStTT0pNopCHjioLgZjPFTGopx+7b6UrtIfU5G8x57896qk1bvlAnfHPPP1qp1rpjJWTRLWomaT NKaTFU9iR3am5paMYptXV0CDil4pKO9Ut9QClpaTFaJpgGaU+1GBihetEtdhBu9qKKUEAdKSfcD3 EjOetRn61KxqFj7140VY6GxjHmo2zTyM85/WmuCelJpokjzzTW604jHXrTSD1ppXAawx3Bpmaeec 80w4ArTXqIY2KjIPWntzTScCm7gMzQRmlJppNNJEjKQ049DTTVANJooNJSAKSl/GkNWkIaRzmg+g oNB6U7IQhpucUtBpXASiiikgAimjrS0dqvQQhHem5546U7mkNNWAguFzEc1mDCnArXkXcprJn+WQ /wAqmSLgNc4BNUZ3B781adztqjJ1NYpXd2NopTg5qsVOauMu6m+WBW6TewrFbYQM0Acc1M44xUZH arjDXUm5asf9Zz0qa6mjUkVTjlMZOKhuHMnOack7j3Q83QzxS+bu71SCNmpAdo60KrbdCsiV261W ZuaczcVGeaiU3J3GkKGLHAp7L8vNNQhaGcmk7PdgRkc0oWlpaiyGNIpOKUmm0wFNJRmigAoBpOlA 5NIDb0EZuOnOK6oHgVzPh9SJW+ldMBWqasTYXNFKKaaEtRCjNJRS+1Go0JUsVR9aenBqWwLApOet Ao7VCeoxDg5php5ptFkUNzUcmCCD0qRqjfofShrTUEcvqKbZjngGs4j0rU1Zf3vWssmtqduUTVmJ SClJpKdtdxC0DrSZpQauIgzR1oxijNJXW4CjNHejtQDmqTVwAnmjpRS5NUtFdgHSgUGjtTTTQrHu TDg8jNRHmpWGO9RkV5XMuhuRn0phPWnNkGmMcc9jRZdRCNwKZmlJyMUw0+VCG5FMbrSseaQnHFUt rANNMIp3ekLHNO3UQw8U00pPJpDRZMBpHvTSMU/Ix7UxjzVpIQhNNNOpvH4U7dhCdKTNKRTe1JoB T04ptLnNN6UwCkpSaQmi6YBRRSdKasICc0UmaKL3YgpMg0GjgCq1uIDg1mXi/OSa0c8VQvvuk+1K RUTLmdVHWqjuT2pJnbcc1GGJqOS2ppcUjvSEgClPA61EzgjFaQb6EvYYxzURJFKzUgra9kSNyTTT 1qQgUxiBUajEZgBULMTQ7ZNNzSnJsAJpCaU0zNZ7AOzRTc0bqQx3SikJpM0AKTSGg0lAATRRRSAK UdaSnxRmRwoqoq7sgNjQ2Pn7exrqgayNK04W8QkbljWsBitGlci4uaM5oo6UrgITS0n86KLDHZFO Q8jnimYpy9eKSBFkeopM5pAeKMc1OhQU00pNNqRoQ8imMcinMDTGzg0r23BHPaqv7zOaySMVsaup Bz2FYzGuqEo8uopiGjrSCindbsQpGBQKO1HQU2lfQQuaQ9aBSgU3drQAoooqlHQAzRmk6GnCiF3o wFAyOaOvFIDgUYqkrKwj3BjTGPH1pzHGaiLeteOrXNxGFRseOlPao2PUd6qSTEITlfpUZpxO04pj dT6UrANY+9ITQx4OKZmrSsTcU9KZQTzSZyDTQDSeelNPWnGmtwfrTshDc80hFL16GkJpvyEMNGeK DSU7a6AGaaaUimk4pPzBA1JRSZpoQtIRR0oPNACUhooJxTQg7UlKDTScdqpINgIzQRQOaQihsAJq pdrmOrVQzLlD60mETlrnKysMY5qANjpV2/UCQ8VmFuTRdplEjScH1qAuSaUk/hTDThe+gCk0A8U3 NMZjmqlJNgPMmKhZ8mg80oSktVZAMwTQUPWpQMUE5qlBdQuRYNNK1KwxTDUOLQDCCO1JUhFNK0rA NoNKRikqRhmjNJRTYBRRRQAZq1ZKWuFx2NVsVoaUm66WqjZO4mdhAAIwPapOe1MjGFHrTi3tVcxP UXkmjvSbqCcU7NBYU8UZzTc5pCc1LY7D92TSocGmDvSp1pX0AtA5FJmlHSkPFRuUheO9Nb0pM0E0 PswEOcdaaeBSk5pp6UrDRi6wDsrBIrodXXMR/OufPWumkroUhlHSlwaMVfJcQUYopadlcBMUZpaB VJWWhIA+1Lik6UtOD7oAxikJpaSnbUApc0Y9aKq1kB7aWAP1phNOOeKjY5JxXiKOpuNbnpTWJ5z+ tKenWoyeDmtNBMQ5JzTSRSluPSmMewouIaeaaVyM0uaaWxmmrMQw8daQmlY5ppNACE80hPvQaQ1V hDTxSZpc0000+wAaQ0UlF2ICaaaXrSHimAlIaM0nWhXYgNFLSZouAZpCaDTTzTsLQOho69aBSE8U AgBoJpDSEiqSFYWmscqR3paQ0OwGDqUWMsByTzxWHIu0109/HlTXOzDDGqu7Gi1RWZiaTrSmkqE7 iGnioyc096iJqtEA8Yxk0Fs0zJNPVaqLXQBpY0ganlKaUpNMAJyKbT9tJtIpPUBtFKRSGi2gDSc0 hFB4NITU3AKSlpKkYUUUUagKK2tDiLTbuwFYo6iuo0WLZDu9auHcTNgUE0A0hNPRE9RQc0UlFPSw 2OzTaOtFSxi5xTkODTM0o4NKwi0p4pSaap4p2eKl6bFDc5pDS000txhTSc0uaaT700rBcztUG6Bu O1c23BNdPqAxA2Oa5lwAx5rSn5Cdxh60nNL1NHet/NMm4hJopeBQKUk76sA60o4pM0tXGSAKBS0g GKq7AdjikwaM0uRin1EIaQZpacFp3BHtTt2HT3qJjjpSknpTGNeS22bWG8800nPfg0E44BqNs4pq wbivUZpxY4phJIPQUWuAYyaafrS5pjGnbohMaaaeeKU0lC0JEY800j1oNIaq1wEPSkNBpCeKcQDN NJBoOAKSgQZ5pCM0Gk6Cq3AKKTNFCQAaTpQaaTSsIcaQ0hJpc0xCdiKbSn60E4oQCGm4pxOaTpTQ BSE0HpRmmIrXSBlNc3eQ7ZTxXVSAEGsK/jCnpwT/AImm9UXExGUelMI5qeQc1AeKyUNRkclQ96mf moTwarRvQQ9cZqUYqAGnBjVLQCbrSEVGHIpPMrTmVgJOKQ0wvmml6bt0Ac1Rk0FqbnNYuTYwNIaX NIRUtAJRRRSAKKKKYE1vGZJVAHeuxs4hFAo74rB0e23uGI6V0i4A9K0tZEvcdmikozSWu47Cg0Gk ozU+QDqbmjOaQnFJXQDhTs0zNANEgLI56dKfnApiEbaWptbUoB1pDRnFBNJ26AJke1IR60ZzSEnp T6AVL0boSBXMSptcjPQ11dyuYm+lctcD9831P86uDbVrDexF0HFAGaQ0A10RatZkBilxSZzS1cVr cQYoBoo4ole+gC0UCjBrTSwhccUE8YpOc0oOaXLzDsAX1pelLSHmnypIR7GW46/rTMj15pWbjrUb HHf8q8mxsKxAOaYTnPpSls0xjxSaSATOKaeT7UhJzSMapXsKwM2aYaUYNNLYNVckQ0hPakOcZzTc kGhNDAnBpCaUnPFNJxQkhCMeKQ0E80U7XEIaSgmmk0egCkUhpM0ZppABoJFJmkzTWggJpM0ZpOnJ p6AKaTNBOabzRYVhSeKTNHWkJxTasrCFzjrSE0hOOaCeKdkx2AnikzSHjmjNNIQMapXcIdCcVbxT JMYINJoaOVul2uQetU261p6mYxKQpyay2ND5UhojaomqRuajYVKGJmlzTKM0AOzQTim0E0rgFITS 0lABmjNITRSuAuaSijNABRRmikMM0+MbnAplPiba4NNAdXpkQitwemRV4HNZFhqEZjCMcEdK1FcM MjpWjJsPJ5ozzTSaAaj0GOzSUmc0ZoAceKODTc0UAOzS5pmaAaNdhlqM5Xvmn7s1FGeBT6htgKT6 UlFNPrQ7WAXGKQ0ZpCQaB2I5huQj2rlrk/vWA7GuolPyNg81y93xcP8AWtKckJogJopOtHSt077k iilzTc4NKTVxndagLuzSZoGDR3qW3YB2eMUA0maSq5nsKw8nPTpQOmaaTS54q+ZBcXrS5xTQe1Ln imm2B6+TjjNMJyTmgtjOPpTGavK6G1wJxkZwKQ0hPHt3pC3FHL1ExCQKaTQWB6U0nmqtdABNMJ60 pOTxTc44otYQbsgU3pmhjgUZyKdkA3NB6UpppOKaQhpNL2pDSE4pAGeKaaUmmk1QhaSgmjNNAJSc UE4OaQnNG24MM0mcmgmkJpX1EFBpM0Zq7gFH6mkzR0oQhp6ilI4ppOOTUL3aL1Io2Am6daaWwaoT anGmeQfxqhNqzH7g4qlGXQNDbeVVHXBrG1PVNibIzzVP7VJLnc1Zl0xLmm4NIEJ5pkfJPJpSKgj4 NW9uUzWL1LSIGphp7DmmGnYBhpKU0lAgpKKM0wCiiikgEopaSk0MSloopAJS0UUWAKWkop2AkSQq etbWnah0R24rBqSOQqRg0aiO0V1YZByKM1zsGpvGuDkj61oW+ppIcMQOO5ppMdkaYIFJUKTo+CrD 86kJGOtIB2aM0gOaKGAtKDSUhOOnJo1aBFqMjbTs+lRRnIqQ0mrFC7uKQmk6UuQRUrYQdqTtQTSU ARy/cNc1qHE54rp36VzmpLiY+tUou+g2UMilFNPFLWiumQHWlFIKOlV1AUUucmm5ozWiaWwDsUdK BR1q0luIUc0dKKTNU0mhWFBoJo60UJNbAetk85phoJprMBXmaGoE0h6UhNITxVc2lkAh4pMkjJop CeOKLMQmcGkJozTSafQBDxSZoJzTciiyEO3U0nmgmkNUhsCaTNBpoNDZIpNIaCwpuetIBe1IDRup KdmIQnnrQTSEc0lNsAJopNygUxpAO+KLgPNNYgDNV5LtEB5GaoT6koyR0p2dwNQyBe9RSXaopJIr DbUGJPNMe63r71SjJhdFy61Q4Ow/lWVNdySEksfzqKWUkmo92a0glcnqPBZ+pp7AKtNj4pJTmjmd 7DsSxcmqd6u2U4qxFJtpl4N3NVPa7EtyktW4jlKqYwasQtXKzRCyIM8VARirDHJqNlzTVgICKYTU xU1GVNKwhtJS0lAAKXNITRQgCiijFDQxKWiigBKWiikAUUUUwClB5pKUUAPBzTgxFR04GgRZjuXT oxq5FqTDhun1rLzSgmmNM6KLUI3HLYNWY5lZeDzXLByKmS5dejGlqwudMGNOB461hx6m/AY1fgvU kABODSSb1Y7aGnGakBFQQyA1PkVCavYYZzSmm0Zp2AUGkzzSE0UNWYATWBqi/vufSt05rE1QHcDW lOVmD20Ms03dzQxzxTabeuhIpODS0EjHvTcnNXJX3Yh1LTSSaRSaacdgHBu1OzjpTcil6VS912bu AtHakzS5quXS4gHSg8UYNHbmkoyWjFc9XJxTQR+NDGmE4NcRoOzRnmm7uKaTTsFhzcGmk0hbPSmk /wA6pp2AXNNLYpc8YppoTEBpDweKQ8UlMBSc0lJnBozxTTADSdqTPNBOOlHxCEYCkNIW5qF7hU46 1DutAfkSkkHnikL4qjJfgAmqcl+XPWrUX0DoarzqBUL3ajoeayZZzgkmqrXJIxmqcdLi1RpT6jtz iqjahIy8n/61Z7ykmoWmzxVqCQPUsTXLFvvEj61A0m4YJqMnK+9JjGM1V32ESK2eKnVvlqqOKlVg aUVITQkpyaaiZNPZc00NtqrWQIe2IxweajZvlpCSzdaH4FF9bMdhqthh7VMcOmKrA4qWNsdavSWg itIu00I2KtSxhwSBVMjaeawnBRLTuT5BoqNXFPzms7pPQYhIpCARSkUYxT1toIiMdMMeKnNIadlY CDYaNpqY000gI9poxTzSUhjCKKeelNo3ASkpc0hpWAKDRRQAUvSgCg0WAM0uc0gpwFMQopRSUYo8 wFooooGKDT1kKng0wUdKaT3EaVtqTw8HkVpw6tEw+Y4Nc3mnK5Heo9nDmuNM69buNgNrAk+hFSBq 5BZ3XoxH41bg1OWI9c1Th2Hc6TJzSgisyHVFkADcGriTIw4YVHK10GTFqydW+6P8+tanGKzNTGU6 UJdBrcwz1ptOI5pDya0S0IEzR1NGM0VVmwFzQMCk6UvanHToIDxzTs5pmeKUVUe4B1NGc0pPHvSZ GKdl3AcDS5poIxQT6ValpqxWPVCcU3NIf0oziuHbRlsDTT1pSc9aQmlKNwAmkJ96Ooppxnmmm9hA TRz603OMmlzz1qtwA03NBpDT0QCnjvTc9u9BOetMZsDND11AXOKgln2555qC4utudpqoZdwJNFmI lkuyM8nnrVN7jf16VBPPtbAOaqvcHt1ra2iuGlixLKCSM1UaXD8VC8hY1GWrWMbCuWGmLZyartJg 9aaWzSdam0m9BD92RUbU/oKYRmqltYEC9etOJzTAecdqUnFTHYYpOKVWxTOtApKetgJwxPSmPkHm noRikcZNW2SJGhJzSTGlViO1RynNKT0vYaWpGDTwajU80+pg7oGiZWGKZJEGHFNBp4Y+ta7oRVIK Hmnq1Sld2c1E0eOlYOnbVFcw7NJnFMORShhWd5XGO603NOyKQ05NgIaQ0GkokwA0lLSVIxCM0mKU 0nSgBppKU0u3NFgG0uKXgUA0WEGaSg9aUCjcAApw4pMc0tOzAKWjFBptNgFANJSijYBaM0hpKalp YB1FIDTqFruAClHWm0ZobAkD4PB4qaK6eM8Maq0ZpuTasCN231XIAfH1p17cRyQcMNx7VhBsUpkO MZqXqik7DmPJptAOaODQ9hB3oINFGaqGwCYopc0ZwaloBM0oNJRQpsQ7FIKTPFFXdAO70vQ02lxT vZaAepE80hP50ZBpCSa5l5jEyaQ80EE03PNUxXFJpOvHeg80gOKVl1HqFJnFBpDSAKQnvSdKQnOK duoCE5qhe3YhXqOenNS3VwIoyc4rl7q6NzLnJx2rSnFMC8sxmbcfwouJ/KjPvxVeFsALnHeoL2TL AZ6VrZc1iVoRNKScmmM+RUWaD0rVxVxBuyaDkUgHNKxxRuroBuaBRnIpRUagLQRxSCnHJFTfqwGY xRmg0lRe+wxaXrSCgVem4h6cGnH2pgOKd1NU0rAIDTJDkU8gVG4oldRAjBwakBBFRjrUgqadxsCO KAcU4UjLW70V0T5Cg0GmdBRuoc0FgIzmm7BT+tBwKjli9WMYVxTeRUnWmtUyjG2gXENNp4PakIqX TVrhcbigiiiosrDG4o206kNS1YBKDQc03vTASilopXGGM07FAFKRTtpcBpFAo6Uo5qVuAuaM5pKK tuwBS0UGloAmaKKKmyAXFGaKKqwBRSUVACmikpcYrTZAGaAaKBUgLmlpuKUUWAcOlGKQGlFNtAHO aSlNAFK19gEpaCKOhoSa3AXpQKQnNAp6c1wFopKKaa5tAPUgaaTzTiaCKyXZjGlsU0nJoxzTaV+w ATRmkPJopWYATSFs0HJFR96pK+4DifyqKVwi5zg05n2jNZt7PkEA0W6MRnapeZGwH61joec0+6ff IajWumnCxLL0ZwuT0FUpnJkarKyHyWzjpiqDZLHnvTk+V7Arj6Kb2pe3vVXBjhx0pppyjAyaaxxz TbutBDacDim9qMVnewxynmnnkZpi8U/PAqou6sIjIpKeabSlDoO4ZxRmk70uRUpaWbAXNKKatO71 py6XADxTGGTTz7Uw0mlazEMK4ozinHkU3FRJWfulDwadnNRCnggVSl3JHY4pjLmnk00im432QEeS OKU5FKetB5GKlXasMTdSZpCKMVPM9hhRR1pKd9RBSmkPFIaluwxTTc0uKQ9aTT3AM0nFGMUEcVIx DRRSgZFKwCilzSUU+lhCE0AGgdacKEAUUGkpt6jFoopDQAhOKcOaSlFSlqAppppTSU5bgFFFITnp SsAoopAadTir7gJRz2pc0lOwDhRSClp6CEJozSHikB9ah7jJAadnFRg07NF7bAOzmjP50maBzVc3 UBO9KKMUYxScXuMKUc02nAcVUFdgeotx1FN3ZpxGajI59BWLuIU5ppGelKRgU0mnYGGeKbml5A54 pM0LQBufz600n9aeRUMrbATS3eoEVzJsQ+prBuJixLFuOtW7664xu5rJd/lJzWsIX1YnfoUXbLda FPFNbrS9q6IATb/3eKqscmpT92oTyamYJkijindqahPSninTXUTE5xTSc089KYetOd2IKTNLg0Yp KNhirTgM02lBrTl0sApNMPJp1N5zU+TEJRS0hGaH5DDNLnmmdDTsmo5r7oAJpDS0hHNFmAmcimnI p4GDSHmk11YDck05aSgClFMCSmnigZoPNXK72EJkmk6Gl6CmnmplotRi96aTR0oNLRqwxKWlHIpQ tCgxXG4zSEU40hpOICCgjNFLTUtNRjTSZp2KCKnVgRkUoOKdSYqGmMTNJS4oIxQ4sAFLSClxQtQC loxRQ009RBSUppKbtbQYUUHJNFSAZpCCadSU7XC4Z96Q0AUUNMLgKdTRmlI4oSAOtAoFAo16gGac OaQUtNK2oCNwKZTyDTMUpXvsA4dKcDTBS0gHmgUlLVpDFHFBPNFFDb2QhCcUoNJRUO6A9TIx+dIy +ppxIprHNQmgGEce9NI4p54pp9aTdwGnOKTHNKaQ+9NIWojHArL1C42ggGr1w+1DzXN6hOzsQDx9 a0ik+oXsVZ5d3U5NVnJKmmsxzzSM2FreLTV0Ii5zTwc0zqaeBTjzNAxH4FRDk1JJwKjXrWbvzWYx 68Gn9KaOadW6dloSBbNJxQTgU0VPM0wHEUlKKDijm7ggpRSClA5qru10AEe1NNPPpmmkUPYLjetI aWkIzUNN7DTEPWnDkU3bS57Uo6bgLTc804CkOPWqbe6EBzTSKUGkNTJ3QwoB5pKUioUmtgHA5ozT Rx0pxHerU21cQhpD9aUc9etBWmk3qFxlFB60lRcYoNKTTKXNJSYWHUHpSDpQaq7UdBBmk6UYopX0 1KFoPNJRnmhyWyEGKSlJpKTl0QxKOtFLUXbASlpKWkAZopKKfM3oAppDS4pKLO+oCCnUlAot3GFB ooodhBSHrSmkApNt7gBNKDmkxRihOwwzSikpcU0wHUCkBoquYQpyeaYTT+1MYUpvqMBxzSk4pMUf SpuMcDTgaaOlANCXUQ40uaQDNGMGnqAtGaTrRUp3A9UYck4pADipDzwaYRio5bbCGFetM5FSE4pn Pekh3GkcU04x1px4NRSHAIq9eWwrlDUJhHEx9BXNSOGLHPJJrX1WbYhX/PasBW7YrelTTjqS2Nbr TWHHNPkqNuRVyjbRIaIxyakWmAc04cdaSXKtAYknNMC5pWNIoJpe65bASLxTulIopSKtRXQQwmkA 55pxFGDQtR3EFLg0KDSngUnFy1FcbnBp2aaRSgcUR5lowHUhGaXtTeRV38gGleaDwKdSEVHK+iC4 zPNKDSMtCisldS1KH0wjIyKcT2pAOMVbbkIBxQMHPrSYxQOuaai+oABg0pozmkPBFJqK1ASnZ4pC TikHNCaT0AcCTS00cCgE1pYBCKTFOJptZSjEExMUmKcRikAzUcrGBpKU0U3qAvam80oozVNcy1AS lAozQDSSVwFIpuM0uaSnOKdgQlLRS1PIAmKMUpNApuK2QCUGjpQBRyaWATNABNFFTrezGFFFLSaV hCd6M0UUrDEOc0tIc5oxzQ7IBc0hopetCSYajaUdaCOeKFp8rQC0opKUUcuuoDsjFMbOadjikatZ LS7C43BNLikHBpax03GApaSihtCFB5p2eKaDTuaSTTAUD1oPFAoxWmltEB6ywwPpUTGpDnpTSuBW Ul2ERHmkz2p7CoyMdPxqVcaGt061TuHwD7VZlbatZ08n7s81W4GDqkhd8Z4FZoqxdyB5jVfNdlNO xA48jpTHGBTwfTpTJKt6LQCPvTgKZk08H5az0aGyNuTQmc+1DdaVeBWUFqBIKVjSLTiQRXQ2rCGG k5zSmmjrWbmlsFh4Ge9ISaAaD0o3jdMBvWnA4pucUUPSzGO3UhNApDVOV0IM0p6ZpBQTS1S3GNJy KBS8U0HnFZyTvdsB2KQmlyaDzVW7AN60oPFHApORSvYBc0nWijFOzYB1pOlBOKTOahvWwC5NKOlI KAeapN21AKCKU0lNxugCkNKaSk2krAGKQ0vakxUSfYYUmaceKbSldaAFApccUU0tNQA0lFFK7BBm iiii7YwNKDxSUU0+ViFJopKKHJ7gGaU9OKbS9qSdwEHWjFAPtS9aW4xKDS0mKADPajn0opM1AC5p Mml6UmDmrsAlOFJ9KUUrsApRSUU9wHimtSikYnFW3oIaKWkFOzmojZjCjFLjFJmiStuAnSng0w5p y0tgHE0ZzRjijFU+Z7AetE8008g08jtTW449etYq8mIiPQ8Uzd1yKlbFQTMFQ1ctNA6FG8l5IzWZ dSFIznpU00u6Vs1mahKR8ueKUE3IHZoyZXyenPrUec0P1xSDiutaOxKQ4MRwKHOcc9qQdM0jDNaP ayAb3pScfSjGKCMCsZRmthkZOTT0IPWmY5p6ripTknsPQkyMcUhoHApCK6G3bYkMU3+KlIpQKxad +wC9qO1AFGKqza2AaRTaH4NMrKTfYqw/OKUVGOtScEcVcJXExcA0HpSChq0WkbgJjNAFB45zSZqN L3AXPak5FJmjdVABo3ZFISDSE4rK7Tuh2HCg00c0E1PNcLC4oxSZozihbXCwtFNLUZobTCw6k70U maGwFxQaTNGaXNfQdgzR0pM0hNLmsFhxNBFMBp1OLugsGaKTNGam/YLCk0UmaTOe9AWHCg0dqDT1 tYAzSZopDRdgOpM0CkY80tbBYdRSCimncApaQ0ZqmraAFFJmlqFcAxmiikA5oae7AU8Ck/SlpDVO 4AT2o6CjFGOKzGJTs02iqTESA0Gmg0uapyurCGmnUnenDpQosYmKOlKBikbirlHS4hM5pRSY5zS1 k7yY7jhS9qSlBq0+jA9cYen86jPWpSSOvamN3rLm8iSNzWdfzCOPC9TV6U7UNc9fzBnZcjntmqvF O4+hD5oGWYgfU1kXsm5zg55q3M+2MnJGc/yrLkfLE8/jW0NNRO5CTSDmhhk0CtYyd7CHA0vUUnQU Z4qpSSYxO9Dfdpveg5xWcpu9gGDk1IKYBk1IODRBMGLnFANHWiqbsxCE0oOOaSgD3pO8mFhc0Z4p KCeKpSsrANZhUZpT1pvWuac20WkGaUNzzTTQKzTdxtExPGaiJJNKWJFJWs6l3YSQZoVuaSgVPP2G OY03NLmkxQ5AgNJS0neoYxRSUUZouIO9KTSUh60J2QWuLThzTRSgc1cXYGK1NJxTjnpTSKJ7iQlL TelKKi4xaTFApaNwEzigUEUtF2gEo70U00LuAuaAcGjtSAUrtgOzSE0p4FNzRdgOzmkNApMU7gKO tBpB1pT1oVwAUZxSCl7UJ2ADzzSUdaBQ2nsAd6XNJj0ooEO7UUgGaWncYUdKWk75ovcAzQSMUdaM U1ZCG49KKOho60gHUtNGaXNVGyAMYNOBptOHIq1UewBnmkYZ70HNNwTUOV+gDqMGgUtOKVtQFHpS gU0dacKHJWstwsf/2Q==";
	//char* pTestStr = &data[0];
	//trim(pTestStr);

    ns1 =
        axiom_namespace_create(env, "http://ws.apache.org/axis2/c/samples",
                               "ns1");
    echo_om_ele =
        axiom_element_create(env, NULL, "root", NULL, &echo_om_node);

	//math result
    text_om_ele = 
		axiom_element_create(env, echo_om_node, "result", NULL, &text_om_node);

	//badge number
	bn_om_ele = 
		axiom_element_create(env, echo_om_node, "badgenumber", NULL, &bntxt_om_node);

	//decode image
	//dec_data = base64_decode(data,strlen(data), pOLength);
	decode(strlen(text),text,100000,dec_data);

	
	//before doing anything, get rid of the default(in) file before creating a new, if it exist
	//it will not ecist if the app was used for the first time, THIS WILL BE USED FOR ENROLLMENT FEATURE
	/*FILE *exist = fopen("image/000faceimage.bmp","r");
	if( exist ) {
		fclose(exist);
		// exists so copy to new file
		filecopy("image/000faceimage.bmp", text2);
		//then remove existing default file
		remove("image/000faceimage.bmp");
		
	} else {
		// doesnt exist
	}*/


	/*out = fopen(output, "w");
    if(out==NULL){
        //cleanup
    }

    n=fwrite(hp,sizeof(char),sizeof(bitmap_header),out);
    if(n<1){
        //cleanup
    }
    fseek(out,sizeof(char)*hp->fileheader.dataoffset,SEEK_SET);
    n=fwrite(data,sizeof(char),hp->bitmapsize,out);
    if(n<1){
        //cleanup
    }*/
	
	FILE *fp;
	//hp=(bitmap_header*)malloc(sizeof(bitmap_header));
	/*FILE *fpDummy;
	//Open input file:
    fpDummy = fopen("image/dummy.bmp", "r+b");
    if(fpDummy==NULL){
        //cleanup
    }

	//Read the input file headers:
    hp=(bitmap_header*)malloc(sizeof(bitmap_header));
    if(hp==NULL) {
	}
        //return 3;

    n=fread(hp, sizeof(bitmap_header), 1, fpDummy);
    if(n<1){
        //cleanup
    }

    //Read the data of the image:
    char *data = (char*)malloc(sizeof(char)*hp->bitmapsize);
    if(data==NULL){
        //cleanup
    }

    fseek(fpDummy,sizeof(char)*hp->fileheader.dataoffset,SEEK_SET);
    n=fread(data,sizeof(char),hp->bitmapsize, fpDummy);
    if(n<1){
        //cleanup
    }*/
	
	if(fp= fopen("faceimage.jpg", "w+b")) {//delete the contents in the file if any exist
		
		/*n=fwrite(hp,sizeof(char),sizeof(bitmap_header),fp);
		if(n<1){
			//cleanup
		}
		fseek(fp,sizeof(char)*hp->fileheader.dataoffset,SEEK_SET);*/
		fwrite(dec_data,1,sizeof(dec_data),fp);
		//convert the input jpeg to a .bmp/.dib




		fclose(fp);
		//fclose(fpDummy);
	}

	//CImg<unsigned char> image("faceimage.jpg");
	//image.load();
	//image.save("000faceimage.bmp");
	cimg::exception_mode(0);
	try{
		CImg<unsigned char> image("faceimage.jpg");
		image.save("_000faceimage.bmp");
	}catch(CImgException &e){
		/*if(err == NFP_NOT_INITIALILZE_ERROR)
		{
			fprintf(stderr, "License Error\n");
		}
		else
		{
			fprintf(stderr, "Unknown exception\n");
		}*/
		return echo_om_node;
	}

	


	//COPY INPUT FILE TO NEW LOCATION
	hp=(bitmap_header*)malloc(sizeof(bitmap_header));
	FILE *in;
	FILE *out;
	//Open input file:
    in = fopen("_000faceimage.bmp", "r+b");
    if(in==NULL){
        //cleanup
    }

	//Read the input file headers:
    /*hp=(bitmap_header*)malloc(sizeof(bitmap_header));
    if(hp==NULL) {
	}
        //return 3;

    n=fread(hp, sizeof(bitmap_header), 1, in);
    if(n<1){
        //cleanup
    }

    //Read the data of the image:
    char *data = (char*)malloc(sizeof(char)*hp->bitmapsize);
    if(data==NULL){
        //cleanup
    }

    fseek(in,sizeof(char)*hp->fileheader.dataoffset,SEEK_SET);
    n=fread(data,sizeof(char),hp->bitmapsize, in);
    if(n<1){
        //cleanup
    }*/


	if(out= fopen("image/_000faceimage.bmp", "w+b")) {//delete the contents in the file if any exist
		
		/*n=fwrite(hp,sizeof(char),sizeof(bitmap_header),out);
		if(n<1){
			//cleanup
		}
		//fseek(fp,sizeof(char)*hp->fileheader.dataoffset,SEEK_SET);
		fwrite(data,1,sizeof(data),out);*/
		int i;
		for(i=getc(in); i!=EOF; i=getc(in)){
			putc(i, out);
		}


		//convert the input jpeg to a .bmp/.dib
		fclose(out);
		fclose(in);
	}
	//eden copy operation

	//start face rec stuff
		int iRet;
		bool Re;
		float score;
		int FindFaceAlg = NFP_ALGORITHM003F2EX;
		
        //fstream outStream;
		// Initialize NeoFacePro
		iRet = Initialize();
		if(iRet == NFP_LICENSE_ERROR)
		{
			fprintf(stderr, "License error\n");
			//return -2;
		}
		else if(iRet != NFP_SUCCESS)
		{
			fprintf(stderr, "initialize error. %d\n", iRet);
			//return -3;
		}

		//CObArray *array = new CObArray();
		//vector<feature *> arrayFeature;

		//Re = EnrollCompoundFeatures(arrayFeature, "image", FindFaceAlg);
		/*if(Re == false)
		{
			fprintf(stderr, "EnrollCompoundFeatures error\n");
			DeleteArray(arrayFeature);
			//return -4;
		}*/

		// verification
		pdata = Verify(FindFaceAlg);
		_s = pdata->score;
		strcpy(text2,pdata->badge);
		/*if(Re == false)
		{
			fprintf(stderr, "Verify error\n");
		    DeleteArray(arrayFeature);
			//return -5;
		}*/

		//DeleteArray(arrayFeature);

		// Terminate NeoFacePro
		iRet = Terminate();
		if(iRet != NFP_SUCCESS)
		{
			fprintf(stderr, "Terminate error. %d\n", iRet);
			//return -6;
		}


	//end face rec sfuff
	sprintf(scoreArray, "%f", _s);
	axiom_element_set_text(text_om_ele, env, (const axis2_char_t*)scoreArray, text_om_node);

	//send the actual badge number -- this stripped from the filename of the enrolled/matched image
	axiom_element_set_text(bn_om_ele, env, text2, bntxt_om_node);

    return echo_om_node;
}

void trim(char* str)
{
    if(!str)
        return;

    char* ptr = str;
    int len = strlen(ptr);

    while(len-1 > 0 && isspace(ptr[len-1]))
        ptr[--len] = 0;

    while(*ptr && isspace(*ptr))
        ++ptr, --len;

    memmove(str, ptr, len + 1);
}

float max_array(float a[], int num_elements)
{
   int i;
   float max=-32000;
   for (i=0; i<num_elements; i++)
   {
	 if (a[i]>max)
	 {
	    max=a[i];
	 }
   }
   return(max);
}

void filecopy(char *fn, char *bn) {
	bitmap_header* hp;
    int n;

	hp=(bitmap_header*)malloc(sizeof(bitmap_header));
	FILE *in;
	FILE *out;
	//Open input file:
    in = fopen(fn, "r+b");
    if(in==NULL){
        //cleanup
    }

	//file name format East+0098345.bmp
	char dpath[100] = "image/East";
	char postfix[30];
	srand ( time(NULL) );
	sprintf(postfix, "%d_", rand() % 30 + 1985);
	strcat(dpath,postfix);
	strcat(dpath, bn);
	strcat(dpath,".bmp");//don't forget the file extension

	if(out= fopen(dpath, "w+b")) {//delete the contents in the file if any exist
		int i;
		for(i=getc(in); i!=EOF; i=getc(in)){
			putc(i, out);
		}


		//convert the input jpeg to a .bmp/.dib
		fclose(out);
		fclose(in);
	}
	//eden copy operation
}

void
set_custom_error(
    const axutil_env_t * env,
    axis2_char_t * error_message)
{
    axutil_error_set_error_message(env->error, error_message);
    //AXIS2_ERROR_SET(env->error, AXIS2_ERROR_LAST + 1, AXIS2_FAILURE);
}

axiom_node_t *
sendImage(
    const axutil_env_t * env,
    axiom_node_t * node)
{
    

    return 0;
}

axiom_node_t *
getResult(
    const axutil_env_t * env,
    axiom_node_t * node)
{
    

    return 0;
}






#ifdef __cplusplus 
}
#endif




////facial rec


/**
 * Serialize the feature which is given as first argment.
 * And output the file which name is given as second argment.
 *
 */
bool SerializeFeature(CFaceFeature *fet,char* fileName) //シリアル化
{
	void *data;
	long size;

	// Serialize the Compound feature. 
	int iRet = fet->Serialize(&data, &size, NFP_SERIALIZETYPE_COMPOUND);
	if(iRet != NFP_SUCCESS)
	{
		fprintf(stderr,"Serialize error.\n");
		return false;
	}

	char cBuf[128];
    FILE* fd = fopen(fileName, "wb");
	if(fd == 0)
	{
		strcpy_s(cBuf, CT2A((LPCTSTR)fileName));
		fprintf(stderr,"[%s] cannot open file.\n",cBuf);
		return false;
	}
	// Output the Compound feature.
	size_t szRet = fwrite(data,1,size,fd);
	if((long)szRet != size)
	{
		strcpy_s(cBuf, CT2A((LPCTSTR)fileName));
		fprintf(stderr,"[%s] cannot write file.\n",fileName);
		return false;
	}

	fclose(fd);

	// Free serealized data.
	fet->FreeSerializeData(data);

	return true;
}

/**
 * Input the specified feature file.
 *
 */
void* ReadTargetFetData(char* fileName) //ターゲットデータを読む
{
#if defined(_WIN32) || defined (_WIN64)
	HANDLE hr = CreateFile((LPCTSTR)fileName,
		                   GENERIC_READ,
						   FILE_SHARE_READ,
						   NULL,
						   OPEN_EXISTING,
						   FILE_ATTRIBUTE_NORMAL,
						   NULL);

	// Get the file size of feature file.
	DWORD size = GetFileSize(hr,NULL);
	CloseHandle(hr);
	if(size < 0)
	{
		return NULL;
	}
#else
    struct stat buf;
    if(stat(fileName, &buf)<0)
    {
        return 0;
    }
	if(buf.st_size < 0)
	{
		return NULL;
	}

	unsigned long size = buf.st_size;
#endif
	// Allocate the memory of feature file.
	char cBuf[128];
	void *data = malloc(size);

	FILE* fd = fopen(fileName, "rb");
	if(fd == 0)
	{
		strcpy_s(cBuf, CT2A((LPCTSTR)fileName));
		fprintf(stderr,"[%s] cannot open file.\n",cBuf);
		return NULL;
	}
	// Read the feature file.
	size_t szRet = fread(data,1,size,fd);
	if(szRet != size)
	{
		strcpy_s(cBuf, CT2A((LPCTSTR)fileName));
		fprintf(stderr,"[%s] cannot read file.\n",cBuf);
		return NULL;
	}

	fclose(fd);

	return data;
}

/**
 * Load the bitmap file.
 *
 */
int LoadBMP(char* pFileName, BITMAPINFO &info, void **body)  //bmpをロードする
{
	char cBuf[128];
	FILE* fh = fopen(pFileName, "rb");

	if (fh <= 0)
	{
		fprintf(stderr,"cannot open file. File=%s detail=%s\n",pFileName,strerror(errno));
		return -1;
	}

	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFO bitmapInfo;
	size_t dwRead;

	ZeroMemory(&bitmapFileHeader,sizeof(bitmapFileHeader));
	ZeroMemory(&bitmapInfo, sizeof(bitmapInfo));

	// Read the file headder.
    if(fread(&bitmapFileHeader.bfType,sizeof(WORD),1,fh)!=1)
    {
        printf("Error(readBmpFileHeader): can not read the bfType of the File Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapFileHeader.bfSize,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpFileHeader): can not read the bfSize of the File Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapFileHeader.bfReserved1,sizeof(WORD),1,fh)!=1)
    {
        printf("Error(readBmpFileHeader): can not read the bfReserved1 of the File Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapFileHeader.bfReserved2,sizeof(WORD),1,fh)!=1)
    {
        printf("Error(readBmpFileHeader): can not read the bfReserved2 of the File Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapFileHeader.bfOffBits,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpFileHeader): can not read the bfOffBits of the File Header.\n");
        fclose(fh);
        return -1;
    }

	// Check if "BM" is exist.(For little endian).
	if(bitmapFileHeader.bfType != 'B'+0x100*'M')
	{
		//　Specific file is not bitmap.
		strcpy_s(cBuf, CT2A((LPCTSTR)pFileName));
		fprintf(stderr,"Illegal Format. File=%s\n",cBuf);
		fclose(fh);
		return -1;
	}

	// Read the bitmap headder.
    fseek(fh,14,SEEK_SET);

    if(fread(&bitmapInfo.bmiHeader.biSize,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biSize of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biWidth,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biWidth of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biHeight,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biHeight of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biPlanes,sizeof(WORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biPlanes of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biBitCount,sizeof(WORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biBitCount of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biCompression,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biCompression of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biSizeImage,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biSizeImage of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biXPelsPerMeter,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biXPelsPerMeter of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biYPelsPerMeter,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biYPelsPerMeter of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biClrUsed,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biClrUsed of the Info Header.\n");
        fclose(fh);
        return -1;
    }

    if(fread(&bitmapInfo.bmiHeader.biClrImportant,sizeof(DWORD),1,fh)!=1)
    {
        printf("Error(readBmpInfoHeader): can not read the biClrImportant of the Info Header.\n");
        fclose(fh);
        return -1;
    }

	info = bitmapInfo;
	if(bitmapInfo.bmiHeader.biBitCount != 24)
	{		
		// 24bit BITMAP.
		fprintf(stderr, "bitmap is not 24bit format");
		return -1;
	}

	// Read the image which image width is multiples of 4.
	/*if((info.bmiHeader.biWidth%4) != 0)
	{
		fprintf(stderr, "bitmap width is invalid");
		return -1;
	}*/

	DWORD dwImageSize = info.bmiHeader.biWidth*
		                info.bmiHeader.biHeight *
					    info.bmiHeader.biBitCount/8;
						 
	*body = malloc(dwImageSize);

	if(body == NULL)
	{
		fprintf(stderr, "malloc failed\n");
		return -1;
	}

	info.bmiHeader.biSizeImage = dwImageSize;

	// Seek
	fseek(fh, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Read the bitmap data.
	dwRead = fread(*body, 1, dwImageSize, fh);

	if(dwImageSize != dwRead)
	{
		// Error about reading file.
		strcpy_s(cBuf, CT2A((LPCTSTR)pFileName));
		fprintf(stderr, "Cannot read file. File=%s\n",cBuf);
		fclose(fh);
		return -1;
	}

	fclose(fh);

	return 1;
}



/**
 * Create Compound feature file.
 *
 */
bool MakeDBFetFile(feature *fet, int iAlg)
{
	// Load the bitmap file.
	char cBuf[128];

	BITMAPINFO binfo;
	void *body;
#if defined(_WIN32) || defined (_WIN64)
    int iRet = LoadBMP(((fet->fileName).GetBuffer()), binfo,&body);
#else
	int iRet = LoadBMP((char*)((fet->fileName).c_str()), binfo,&body);
#endif
	if(iRet < 0)
	{
		strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stderr, "[%s] cannot open file.\n", cBuf);
		return false;
	}

	CFaceInfo *info;
	try{
		info = new CFaceInfo();
	}catch(int err){
		if(err == NFP_NOT_INITIALILZE_ERROR)
		{
			fprintf(stderr, "License Error\n");
		}
		else
		{
			fprintf(stderr, "Unknown exception\n");
		}
		free(body);
		return false;
	}
	
	// Set the detection parameter.
	info->SetParamAlgorithm(iAlg);
	info->SetParamEyesRoll(15);
	info->SetParamEyesMaxWidth(240);
	info->SetParamEyesMinWidth(30);
	info->SetParamReliability(0.5f);

	// Face detection.
	iRet = info->FindFace(binfo, body);
	if(iRet == NFP_CANNOT_FIND_FACE)
	{
		// Face did not detected.
		strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stdout, "[%s] cannot find face.\n", cBuf);
		delete info;
		free(body);
		return false;
	}
	else if(iRet == NFP_SUCCESS)
	{
		
		CFaceFeature *ffet = new CFaceFeature();

		// Create the Simplex feature.
		iRet = ffet->SetFeature(info);
		if(iRet != NFP_SUCCESS)
		{
			strcpy_s(cBuf, CT2A(fet->fileName));
			fprintf(stderr, "[%s] make feature error. %d\n", cBuf, iRet);
			delete info;
			delete ffet;
			free(body);
			return false;
		}



		// Get the eye positions.
		POINT L = info->GetLeftEye();
		POINT R = info->GetRightEye();	

		strcpy_s(cBuf, CT2A(fet->name));
		fprintf(stdout,"%s  L:%d,%d  R:%d,%d\n",cBuf, L.x, L.y, R.x, R.y);

        CString fetFile;
		// Output the Compound feature file.
#if defined(_WIN32) || defined (_WIN64)
		fetFile.Format(_T("feature\\%s.fet"), fet->name);
        bool Ret = SerializeFeature(ffet, (char*)fetFile.GetBuffer());
#else
        fetFile = "feature/" + fet->name + ".fet";
		bool Ret = SerializeFeature(ffet, (char*)fetFile.c_str());
#endif

		if(Ret == false)
		{
			fprintf(stderr, "serialize error\n");
			delete info;
			delete ffet;
			free(body);
			return false;
		}
		delete info;
        delete ffet;
		free(body);
		fet->fetFileName = fetFile;	
	}
	else{
		strcpy_s(cBuf, CT2A(fet->name));
        fprintf(stderr,"[%s] find face error. %d\n", cBuf, iRet);
	    delete info;
		free(body);
		return false;
	}

	return true;
}

/**
 * Create the Feature file for target side.
 * This feature file can use for entry in the DB.
 *
 */
bool EnrollCompoundFeatures(vector<feature *> &Features, char* lpPath, int iAlg)  //複合特徴を記入する
{
	CString str;
	bool iRet;

	Features.clear();
	//Find the Bitmap file.
	str = lpPath;
#if defined(_WIN32) || defined (_WIN64)
	str = str + "/*.bmp";
	//str = "../*.bmp";
	WIN32_FIND_DATA ffData; 
	HANDLE hr = FindFirstFile(str, &ffData);
	if(hr != INVALID_HANDLE_VALUE)
    {
		do
#else
    struct dirent* _dirent = 0;
	DIR *dir;
    dir =opendir(str.c_str());
    if (dir != 0)
    {
        while ((_dirent=readdir(dir))!= 0)
#endif
        {
#if defined(_WIN32) || defined (_WIN64)
#else
			int nameLength = strlen(_dirent->d_name);
			if(nameLength >= 5 &&
				(_dirent->d_name[nameLength-3] == 'b' || _dirent->d_name[nameLength-3] == 'B') &&
				(_dirent->d_name[nameLength-2] == 'm' || _dirent->d_name[nameLength-2] == 'M') &&
				(_dirent->d_name[nameLength-1] == 'p' || _dirent->d_name[nameLength-1] == 'P') 
			)
			{
			}
			else
			{
				continue;
			}
#endif
			feature *fet = new feature();

#if defined(_WIN32) || defined (_WIN64)
			fet->name = ffData.cFileName;
#else
			fet->name = _dirent->d_name;
#endif
			fet->fileName = lpPath;
			fet->fileName += "/" + fet->name;

			//Output the Compound file.
			iRet = MakeDBFetFile(fet, iAlg);
			if(iRet == true)
			{
				Features.push_back(fet);
			}
			else
            {
				delete fet;
				fet = NULL;
			}
		} 
#if defined(_WIN32) || defined (_WIN64)
		while( FindNextFile(hr, &ffData) );
#endif
	} else {
        fprintf(stderr, "Find First File Err\n");
        return false;
	}
	
#if defined(_WIN32) || defined (_WIN64)
    FindClose(hr);
#else
	closedir(dir);
#endif

	if(Features.size() == 0)
	{
        fprintf(stderr, "No Image enroll success\n");
		return false;
	}

	return true;
}

/**
 * Load the feature.
 * Load the feature.
 *
 */
bool LoadFeatures(vector<feature *> &Features, char* lpPath, int iAlg)  //複合特徴を記入する
{
	CString str;
	bool iRet;

	Features.clear();
	//Find the Bitmap file.
	str = lpPath;
#if defined(_WIN32) || defined (_WIN64)
	str = str + "/*.bmp";
	//str = "../*.bmp";
	WIN32_FIND_DATA ffData; 
	HANDLE hr = FindFirstFile(str, &ffData);
	if(hr != INVALID_HANDLE_VALUE)
    {
		do
#else
    struct dirent* _dirent = 0;
	DIR *dir;
    dir =opendir(str.c_str());
    if (dir != 0)
    {
        while ((_dirent=readdir(dir))!= 0)
#endif
        {
#if defined(_WIN32) || defined (_WIN64)
#else
			int nameLength = strlen(_dirent->d_name);
			if(nameLength >= 5 &&
				(_dirent->d_name[nameLength-3] == 'b' || _dirent->d_name[nameLength-3] == 'B') &&
				(_dirent->d_name[nameLength-2] == 'm' || _dirent->d_name[nameLength-2] == 'M') &&
				(_dirent->d_name[nameLength-1] == 'p' || _dirent->d_name[nameLength-1] == 'P') 
			)
			{
			}
			else
			{
				continue;
			}
#endif
			feature *fet = new feature();

#if defined(_WIN32) || defined (_WIN64)
			fet->name = ffData.cFileName;
#else
			fet->name = _dirent->d_name;
#endif
			fet->fileName = lpPath;
			fet->fileName += "/" + fet->name;


			 CString fetFile;
		// Output the Compound feature file.
#if defined(_WIN32) || defined (_WIN64)
		fetFile.Format(_T("feature\\%s.fet"), fet->name);
       // bool Ret = SerializeFeature(ffet, (char*)fetFile.GetBuffer());
#else
        fetFile = "feature/" + fet->name + ".fet";
		//bool Ret = SerializeFeature(ffet, (char*)fetFile.c_str());
#endif
			//Output the Compound file.
			/*iRet = MakeDBFetFile(fet, iAlg);
			if(iRet == true)
			{
				Features.push_back(fet);
			}
			else
            {
				delete fet;
				fet = NULL;
			}*/
		fet->fetFileName = fetFile;
		Features.push_back(fet);
		} 
#if defined(_WIN32) || defined (_WIN64)
		while( FindNextFile(hr, &ffData) );
#endif
	} else {
        fprintf(stderr, "Find First File Err\n");
        return false;
	}
	
#if defined(_WIN32) || defined (_WIN64)
    FindClose(hr);
#else
	closedir(dir);
#endif

	if(Features.size() == 0)
	{
        fprintf(stderr, "No Image enroll success\n");
		return false;
	}

	return true;
}

/**
 * Create the feature for query side.
 *
 */
CFaceFeature* MakeQueryFetObj(feature *fet, int iAlg)
{
	// Load the bitmap file.
	char cBuf[128];

	BITMAPINFO binfo;
	void *body;
#if defined(_WIN32) || defined (_WIN64)
    int iRet = LoadBMP((char*)((fet->fileName).GetBuffer()), binfo, &body);
#else
	int iRet = LoadBMP((char*)((fet->fileName).c_str()), binfo,&body);
#endif

	if(iRet < 0)
	{
        strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stderr, "[%s] cannot open file.\n", cBuf);
		return NULL;
	}

	CFaceInfo *info;

	try{
		info = new CFaceInfo();
	}catch(int err){
		if(err == NFP_NOT_INITIALILZE_ERROR)
		{
			fprintf(stderr, "License Error\n");
		}
		else
		{
			fprintf(stderr, "Unknown exception\n");
		}
		free(body);
		return NULL;
	}

	// Set the detection parameter.
	info->SetParamAlgorithm(iAlg);
	info->SetParamEyesRoll(15);
	info->SetParamEyesMaxWidth(240);
	info->SetParamEyesMinWidth(30);
	info->SetParamReliability(0.5f);

	// Face detection.
	iRet = info->FindFace(binfo, body);
	if(iRet == NFP_CANNOT_FIND_FACE)
	{
		// Face did not detected.
		strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stdout, "[%s] cannot find face.\n", cBuf);
	    free(body);
		delete info;
		return NULL;
	}
	else if(iRet != NFP_SUCCESS)
	{
		strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stderr, "[%s] find face error. %d\n", cBuf, iRet);
		free(body);
		delete info;
		return NULL;
	}

	CFaceFeature *ffet = new CFaceFeature();

	// Create the feature.
	iRet = ffet->SetFeature(info);
	if(iRet != NFP_SUCCESS)
	{
        strcpy_s(cBuf, CT2A(fet->fileName));
		fprintf(stderr, "[%s] make feature error. %d\n", cBuf, iRet);
	    free(body);
		delete info;
		delete ffet;
		return NULL;
	}

    free(body);
	delete info;
	return ffet;
}

/**
 * Execute face verification
 */
recognition_data *Verify(int iAlg)
{

	// Create the object for face verification.
	CVerifier *verify;
	char cBuf[128];
	char tBuf[128];
	bool flag = false;
	float score;
	//float allScores[1000];
	int s = -1;
	float threshold = 0.60;
	float maxScore;
	//verify = new CVerifier();
	try{
		verify = new CVerifier();
	}catch(int err){
		if(err == NFP_NOT_INITIALILZE_ERROR)
		{
			fprintf(stderr, "License Error\n");
		}
		else
		{
			fprintf(stderr, "Unknown exception\n");
		}
		return false;
	}

       vector<feature *> arrayFeature;

	/*if(LoadFeatures(arrayFeature,"image",iAlg)) {
		arrayFeature = arrayFeature;
	}*/

	LoadFeatures(arrayFeature,"image",iAlg);

	//Create the feature for query side. 
	//feature *fet = features[0];
	feature *fet = NULL;
       int fCount = (int)arrayFeature.size()-1;

	int c;
	for(c = 0; c <= fCount;c++) {
		strcpy_s(tBuf, CT2A(arrayFeature[c]->name));
		if(strcmp(tBuf,"_000faceimage.bmp") == 0) {
			fet = arrayFeature[c];
		}
	}

	CFaceFeature *query;
    query = MakeQueryFetObj(fet, iAlg);
	if(query==NULL)
	{
		strcpy_s(cBuf, CT2A(fet->fetFileName));
	    fprintf(stderr, "ReadTargetFetData failed. %s\n", cBuf);
		delete verify;
		return false;
	}
	strcpy_s(cBuf, CT2A(fet->name));
	fprintf(stdout, "\nTarget/Query\n");
	fprintf(stdout, "           ");
	fprintf(stdout, "%11s", cBuf);
	
	//Get the number of files of the list.
	int iMax = (int)arrayFeature.size()-1;
	//allocate the sapce required for my scores array
	float *allScores = (float *) malloc(iMax*sizeof(float));
	recognition_data *data = (recognition_data *) malloc(1*sizeof(recognition_data));

	int iRet, i;
	for(i = 0; i <= iMax;i++)
	{
		feature *fet = arrayFeature[i];

		// Read the feature data of target side from file.
		void *target;
		fprintf(stdout, "\n");
#if defined(_WIN32) || defined (_WIN64)
        target = ReadTargetFetData((char *)(fet->fetFileName).GetBuffer());
#else
		target = ReadTargetFetData((char *)(fet->fetFileName).c_str());
#endif
		if(target < 0)
		{
			strcpy_s(cBuf, CT2A(fet->fetFileName));
			fprintf(stderr, "ReadTargetFetData failed. %s\n", cBuf);
			delete verify;
			delete query;
			return false;
		}
		strcpy_s(cBuf, CT2A(fet->name));
		fprintf(stdout, "%-11s", cBuf);

		// Verification
		//float score;
		iRet = verify->Verify(query, target, &score);
		if(iRet != NFP_SUCCESS)
		{
			fprintf(stderr, "Verify error. %d\n", iRet);
			delete verify;
			free(target);
			delete query;
			return false;
		}
		

		if(strcmp(cBuf,"_000faceimage.bmp") != 0) {
			//store lastest score
			//allScores[i] = score;
			maxScore = score;
		}

		if(maxScore < threshold && flag != true) {
			//score = 0.00000;
			data->score = 0.00000;  //meaning no match was found
			data->badge = "00000";
		}else {
			flag = true;
			char badge[5];
			char *tok;
			int i = 0;
			char tmpbuf[128];
			//only if the next test is greater than the score, should we do anything
			if(data->score < maxScore) {
				data->score = maxScore; //match was found and the max will be whatever the highest score or if above threshold
				//score = maxScore;
				//sprintf(badge, "%d_", fet->name);
				strcpy(tmpbuf,cBuf);
				tok = strtok (tmpbuf,"_.");
				while (tok != NULL && i != 3)
				{
					i++;
					//

					//tok = strtok (NULL, " _.");
					if(i != 2){
						continue;
					}else{
						tok = strtok (NULL, " _.");
					}
				}


				data->badge = (axis2_char_t *)tok;
				i = 0;
			}
		}

		fprintf(stdout, "%11f", data->badge);
		fprintf(stdout, "%11f", score);
		free(target);

	}
	//end loop for retriveing the score

	//get MAX score cause that is all we care about
	//float maxScore = max_array(allScores, iMax);

	/*if(maxScore < threshold) {
		//score = 0.00000;
		data->score = 0.00000;
	}else {
		data->score = maxScore;
		//score = maxScore;
	}*/

	//store globally
	//recognition_data pdata = data;

    fprintf(stdout, "\n\n");
	free(allScores);
	//free(data);
	delete query;
	delete verify;
	return data;
}

void DeleteArray(vector<feature *> arrayFeature)  //メモリを解放する
{
	feature * temp;
	for(unsigned int i=0; i < arrayFeature.size();i++)
	{
		temp = arrayFeature[i];
		delete temp;
	}
	arrayFeature.clear();
}