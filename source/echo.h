
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef CALC_H
#define CALC_H

//#define _WIN32
//#define ddsf

#include <axis2_svc_skeleton.h>
#include <axutil_log_default.h>
#include <axutil_error_default.h>
#include <axiom_text.h>
#include <axiom_node.h>
#include <axiom_element.h>


#ifdef __cplusplus  
	extern "C"{ 
#endif

axiom_node_t *axis2_echo_echo(
    const axutil_env_t * env,
    axiom_node_t * node);
#ifdef __cplusplus 
}
#endif

typedef struct {
    char         filetype[2];   /* magic - always 'B' 'M' */
    unsigned int filesize;
    short        reserved1;
    short        reserved2;
    unsigned int dataoffset;    /* offset in bytes to actual bitmap data */
} file_header;

/* Windows 3.x bitmap full header, including file header */
typedef struct {
    file_header  fileheader;
    unsigned int headersize;
    int          width;
    int          height;
    short        planes;
    short        bitsperpixel;  /* we only support the value 24 here */
    unsigned int compression;   /* we do not support compression */
    unsigned int bitmapsize;
    int          horizontalres;
    int          verticalres;
    unsigned int numcolors;
    unsigned int importantcolors;
} bitmap_header;

/*holds pertinent info regarding a facial match*/
typedef struct {
	float score;//score that indicates if a match was found
	axis2_char_t *  badge;//badge of the person being recognized
	unsigned int empId;//employess id 
	char *name;//individuals name
} recognition_data;

/**/
/*axiom_node_t *sendImage(
    const axutil_env_t * env,
    axiom_node_t * node);*/

/**/
/*axiom_node_t *getResult(
    const axutil_env_t * env,
    axiom_node_t * node);*/




#endif                          /* CALC_H */
